package com.infostretch.spring.controller;


import java.text.DateFormat;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.infostretch.spring.model.Employee;
import com.infostretch.spring.service.EmployeeService;

//import com.infostretch.spring.model.User;

@Controller
public class HomeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@RequestMapping(value="/")
	public String home(Locale locale,Model model) {
		System.out.println("Home page requested,locale"+locale);
		java.util.Date date=new java.util.Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate);
		return "home";
	}
	
	@RequestMapping("/employees")
	public ModelAndView employees(Model model) {
		System.out.println("employee page requested");
		//model.addAttribute("employeesList",employeeService.getEmployees());
		return new ModelAndView("employees","employeesList",employeeService.getEmployees());
	}
	
	@RequestMapping(value = "/Delete/{id}")
	public String delete(@PathVariable Integer id) {
		employeeService.delete(id);
		return "redirect:/employees";
	}
	
}
