package com.infostretch.spring.service;

import java.util.List;

import com.infostretch.spring.model.Employee;

public interface EmployeeService {
	List<Employee> getEmployees();
	void delete(Integer id);
}
