package com.infostretch.spring.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.infostretch.spring.model.Employee;


@Service
public class EmployeeImpl implements EmployeeService {
	private Map<Integer,Employee> employees;
	
	@Override
	public List<Employee> getEmployees() {
		return new ArrayList<>(employees.values());
	}
	
	public EmployeeImpl() {
		loadEmployees();
	}

	private void loadEmployees() {
		employees=new HashMap<Integer, Employee>();
		Employee employee=new Employee();
		employee.setId(111);
		employee.setName("Abhijit");
		employee.setDesignation("Associate engineer");
		employees.put(1, employee);
		Employee employee2=new Employee();
		employee2.setId(222);
		employee2.setName("Aniket");
		employee2.setDesignation("Senior SE");
		employees.put(2, employee2);
		Employee employee3=new Employee();
		employee3.setId(333);
		employee3.setName("Ankita");
		employee3.setDesignation("Project manager");
		employees.put(3, employee3);
	}

	@Override
	public void delete(Integer id) {
		employees.remove(id);
	}
}
