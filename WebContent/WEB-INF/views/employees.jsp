<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>
	<h1>Employee page</h1>
	<table>
      <thead>
        <tr>
            <td>ID</td>
            <td>NAME</td>
            <td>DESIGNATION</td>
            <td>Delete</td>
        </tr>
      </thead>
      <tbody>
        <c:forEach  items="${employeesList}" var="e">
            <tr>
                <td>${e.id}</td>
                <td>${e.name}</td>
                <td>${e.designation}</td>
                <td><button type="button">Delete</button></td>
            </tr>
        </c:forEach>
      </tbody>
  </table>
  </center>
</body>
</html>